public class main {
    public static void main(String...args){
        Dog dog=new Dog();
        print(dog);
        Cat cat=new Cat();
        print(cat);
        Frog frog=new Frog();
        print(frog);
    }
    private  static void print(Animals animals){
        String str =
                animals.getName() +
                        " говорит " +
                        animals.getVoice();
        System.out.println(str);
    }
}
