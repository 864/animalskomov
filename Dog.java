public class Dog implements Animals {
    @Override
    public String getName() {
        return "Собака";
    }

    @Override
    public String getVoice() {
        return "Гав-гав";
    }
}
