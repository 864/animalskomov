public class Frog implements Animals {
    @Override
    public String getName() {
        return "Лягушка";
    }

    @Override
    public String getVoice() {
        return "Ква-ква";
    }
}
