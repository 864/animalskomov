public class Cat implements Animals {
    @Override
    public String getName() {
        return "Кошка";
    }

    @Override
    public String getVoice() {
        return "Мяу";
    }
}
